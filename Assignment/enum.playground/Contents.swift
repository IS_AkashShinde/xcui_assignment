import UIKit

enum SocialNetwork: String {
    case facebook = "the social dilemma"
    case instagram = "filters"
    case twitter = "controversies"
    case linkedIn = "job post"
}

  func shareImage(on platform : SocialNetwork){
    switch platform{
        
    case .facebook:
        print("share image on facebook.")
    case .instagram:
        print("share image on instagram.")
    case .twitter:
        print("share image on twitter.")
    case .linkedIn:
        print("share image on linkedIn.")
    
    }
}

shareImage(on: .instagram)

func rawEnum(on platform : SocialNetwork){
    print(platform.rawValue)
}
rawEnum(on: .linkedIn)
