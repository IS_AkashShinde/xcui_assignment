import UIKit

let wholeNumber = Int("27")

let lessPrecisePI = Float("3.14")

let morePrecisePI = Double("3.14545")

var number = Double("3.1415")

if let number = number {
    print("the number is \(number)!")
}else{
    print("the value is not valid")
}

// Assignment
// conversion of int to float

let myInt : Int = 123

let myFloat = Float(myInt)

print(myFloat)

// conversion of float into int

let  float: Float = 10.25

let int = Int(float)

print(int)

// Conversion of string into integer

let myString = "553"

let myInt1 = Int(myString)
print(myInt1)
 
let myInt2 = Int(myString) ?? 0
print(myInt2)



