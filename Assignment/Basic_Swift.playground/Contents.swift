import UIKit

var str = "Hello, playground"

let char = "b"

if char == "b"{
    print("character is a")
}
else{
    print("not character a")
}

switch char {
case "a":
    print("this is an a")
case "b", "c":
    print("this is b and c")
default:
    print("this is not a")
}

//loops - iteration loop
var sum = 0
for counter in 1...5{
    print(counter)
    print("hello")

//addition of 1 to 5 numbers
    sum += counter
}
 print(sum)
