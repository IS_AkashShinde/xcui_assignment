import UIKit

var str = "Hello, playground"

// function keyword
func addTwoVar(){
    let a = 2
    let b = 3
    
    let c = a + b
    print(c)
    
}

func multiTwoVar(){
   let d = 8
   let e = 3
   
   let f = d * e
   print(f)
}
    
addTwoVar()
multiTwoVar()

// function using return
func addTwoNumber() -> Int {
    let a = 2
    let b = 3
    
    let c = a + b
    return c
    
}
 let sum = addTwoNumber()
    print(sum)

//function using parameter
func multiTwoNumber(arg par: Int) -> Int {
   let d = par
   let e = 3
   
   let f = d * e
   return f
}
    let twonumbers = multiTwoNumber(arg: 8)
    print(twonumbers)
