import UIKit

// fibonacci with function keyword
func fibForNums(numSteps: Int) -> [Int]{
    
    var sequence = [0, 1]
    for _ in 0...numSteps {
        let first = sequence[sequence.count - 2]
        let second = sequence.last!
        sequence.append(first + second)
    }
    return sequence
}

var fibSeries = fibForNums(numSteps: 5)
print(fibSeries)

// fibonacci without function keyword
var x = -1 , y = 1, sum = 0
for i in 0..<5 {
    sum = x + y
    x = y
    y = sum
    print(sum)
}


// String assignment

var input = "GameOfThrones"
//reverse string
var reversed = String(input.reversed())
print(reversed)

// upper and lowercase string
print(input.lowercased())
print(input.uppercased())

//convert "ones" onto "Ones"
let replace = input.replacingOccurrences(of: "ones", with: "Ones")
print(replace)





