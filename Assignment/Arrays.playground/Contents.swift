import UIKit

var a = "Cat"
var b = "Dog"
var c = "Parrot"

a = "my " + a

var myArray = ["Cat", "Dog", "Parrot"]
print(myArray[1])

for counter in 0...2{
    print(myArray[counter])
}

myArray.insert("Frog", at: 0)
myArray.append("Billa")
myArray  += ["Bird", "Bear"]

for item in myArray {
    print(item)
}

myArray.remove(at: 2)
myArray.reverse()


var myDictionary = [String:String]()

myDictionary["3019"] = "aKASH"
myDictionary["3020"] = "chandan"

let eml = myDictionary["3020"]
for(key, value) in myDictionary {
    print("\(key) is a \(value)")
}
