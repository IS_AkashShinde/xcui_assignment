import UIKit


class Person {
    var name = ""
    
    init() {
    }
    init(_ name: String) {
        self.name = name
    }
}

class Employee: Person {
    
    
    var salary = 0
    var role = ""
    
    func work(){
        print("my name is  \(name) and  working on xcui test ticket")
        salary += 1
    }
}

class Managers : Employee{
   var teamsize = 0
    
      override func work() {
        super.work()
          print("managing people")
        salary += 2
      }
    
}

var m = Managers()
m.name = "Akash"
m.salary = 2000
m.role = "SE"
m.teamsize = 4
m.work()

var mone = m
mone.name = "vijay"
print(m.name)

let p = Person("tom")
print(p.name)

let e = Employee("jerry")
print(e.name)


struct Item {
    var name = ""
    
    init() {
    }
    init(_ name: String) {
        self.name = name
    }
}

var itemlist = Item("Girish")
var newItem = itemlist
newItem.name = "Sanam"
print(itemlist.name)
